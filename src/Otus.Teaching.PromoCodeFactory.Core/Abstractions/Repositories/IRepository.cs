﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IList<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<T> AddAsync(T item);

        Task<bool> DeleteAsync(T item);
    }
}